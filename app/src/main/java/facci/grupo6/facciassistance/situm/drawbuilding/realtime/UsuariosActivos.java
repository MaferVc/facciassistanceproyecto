package facci.grupo6.facciassistance.situm.drawbuilding.realtime;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import es.situm.sdk.SitumSdk;
import es.situm.sdk.error.Error;
import es.situm.sdk.location.LocationListener;
import es.situm.sdk.location.LocationManager;
import es.situm.sdk.location.LocationRequest;
import es.situm.sdk.location.LocationStatus;
import es.situm.sdk.model.cartography.Building;
import es.situm.sdk.model.cartography.Floor;
import es.situm.sdk.model.location.Bounds;
import es.situm.sdk.model.location.Coordinate;
import es.situm.sdk.model.location.Location;
import es.situm.sdk.model.realtime.RealTimeData;
import es.situm.sdk.realtime.RealTimeListener;
import es.situm.sdk.realtime.RealTimeRequest;
import es.situm.sdk.utils.Handler;
import facci.grupo6.facciassistance.R;
import facci.grupo6.facciassistance.situm.drawbuilding.GetBuildingImageUseCase;


public class UsuariosActivos
        extends AppCompatActivity
        implements OnMapReadyCallback {


    private static final String TAG = UsuariosActivos.class.getSimpleName();

    private static final double DISTANCE_CHANGE_TO_ANIMATE = 0.2;
    private static final int BEARING_CHANGE_TO_ANIMATE = 1;
    private static final int DURATION_POSITION_ANIMATION = 500;
    private static final int DURATION_BEARING_ANIMATION = 200;

    private GoogleMap map;
    private Marker prev;
    private ProgressBar progressBar;
    private GetBuildingImageUseCase getBuildingImageUseCase = new GetBuildingImageUseCase();
    private GetBuildingsUseCase getBuildingsUseCase = new GetBuildingsUseCase();

    //para que solo se inicie el posicionamiento dentro del edificio

    private String buildingId = "4504";
    //=1

    private LocationManager locationManager;
    private LocationListener locationListener;
    private Location current;
    private Location lastLocation;
    private Building building;
    private LatLng destinationLatLng;
    private LatLng lastLatLng;
    private List<Marker> markers = new ArrayList<>();

    private float lastBearing;
    private float destinationBearing;
    private boolean markerWithOrientation = false;

    private ValueAnimator locationAnimator = new ValueAnimator();
    private ValueAnimator locationBearingAnimator = new ValueAnimator();

    FloatingActionButton button;
    private TextView noDevicesTV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuarios_activos);

        SitumSdk.init(this);



        locationManager = SitumSdk.locationManager();
//
        Intent intent = getIntent();
        if (intent != null)
            if (intent.hasExtra(Intent.EXTRA_TEXT))
                buildingId = intent.getStringExtra(Intent.EXTRA_TEXT);
//del boton

        setup();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        SitumSdk.communicationManager().fetchBuildings(new Handler<Collection<Building>>() {
            @Override
            public void onSuccess(Collection<Building> buildings) {
                Log.d(TAG, "onSuccess: Your buildings: ");
                for (Building building : buildings) {
                    Log.i(TAG, "onSuccess: " + building.getIdentifier() + " - " + building.getName());
                }
            }

            @Override
            public void onFailure(Error error) {
                Log.e(TAG, "onFailure:" + error);
            }
        });


    }


    @Override
    protected void onDestroy() {
        getBuildingImageUseCase.cancel();
        SitumSdk.locationManager().removeUpdates(locationListener);

        SitumSdk.realtimeManager().removeRealTimeUpdates();
        super.onDestroy();
    }


    @Override
    public void onMapReady(final GoogleMap googleMap) {
        map = googleMap;
        getBuildingImageUseCase.get(buildingId, new GetBuildingImageUseCase.Callback() {
            @Override
            public void onSuccess(Building build, Floor floor, Bitmap bitmap) {
                progressBar.setVisibility(View.GONE);
                building = build;
                drawBuilding(building, bitmap);
                realtime(building);

            }

            @Override
            public void onError(Error error) {
                Toast.makeText(UsuariosActivos.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        this.map = googleMap;
        getBuildingsUseCase.get(new GetBuildingsUseCase.Callback() {
            @Override
            public void onSuccess(List<Building> buildings) {
                hideProgress();
                if (buildings != null && !buildings.isEmpty()) {
                    Building building = buildings.get(0);
                    realtime(building);
                }
            }

            @Override
            public void onError(String error) {
                hideProgress();
                Toast.makeText(UsuariosActivos.this, error, Toast.LENGTH_LONG).show();
            }
        });
    }


    private void realtime(Building building) {
        RealTimeRequest realTimeRequest = new RealTimeRequest.Builder()
                .pollTimeMs(3000)
                .building(building)
                .build();
        SitumSdk.realtimeManager().requestRealTimeUpdates(realTimeRequest, new RealTimeListener() {
            @Override
            public void onUserLocations(RealTimeData realTimeData) {
                if(realTimeData.getLocations().isEmpty()){
                    noDevicesTV.setVisibility(View.VISIBLE);
                    for (Marker marker : markers) {
                        marker.remove();
                    }
                    markers.clear();
                }else {
                    noDevicesTV.setVisibility(View.GONE);
                    for (Marker marker : markers) {
                        marker.remove();
                    }
                    markers.clear();
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    for (Location location : realTimeData.getLocations()) {
                        LatLng latLng = new LatLng(location.getCoordinate().getLatitude(),
                                location.getCoordinate().getLongitude());
                        MarkerOptions markerOptions = new MarkerOptions()
                                .position(latLng)
                                .title(location.getDeviceId());
                        Marker marker = map.addMarker(markerOptions);
                        markers.add(marker);
                        builder.include(latLng);
                    }
                    try {
                        map.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 100));
                    } catch (IllegalStateException e) {
                    }
                }
            }

            @Override
            public void onError(Error error) {
                Toast.makeText(UsuariosActivos.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }


    private void setup() {
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        button = (FloatingActionButton) findViewById(R.id.start_button);
        noDevicesTV = (TextView) findViewById(R.id.rt_nodevices_tv);

        View.OnClickListener buttonListenerLocation = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(UsuariosActivos.class.getSimpleName(), "button clicked");
                if(locationManager.isRunning()){
                    stopLocation();
                    SitumSdk.locationManager().removeUpdates(locationListener);
                }else {
                    markerWithOrientation = false;
                    startLocation();
                }
            }
        };
        button.setOnClickListener(buttonListenerLocation);
    }


    private void drawBuilding(Building building, Bitmap bitmap){
        Bounds drawBounds = building.getBounds();
        Coordinate coordinateNE = drawBounds.getNorthEast();
        Coordinate coordinateSW = drawBounds.getSouthWest();
        LatLngBounds latLngBounds = new LatLngBounds(
                new LatLng(coordinateSW.getLatitude(), coordinateSW.getLongitude()),
                new LatLng(coordinateNE.getLatitude(), coordinateNE.getLongitude()));

        map.addGroundOverlay(new GroundOverlayOptions()
                .image(BitmapDescriptorFactory.fromBitmap(bitmap))
                .bearing((float) building.getRotation().degrees())
                .positionFromBounds(latLngBounds));

        map.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 100));
    }


    private void startLocation() {
        if (locationManager.isRunning()) {
            return;
        }

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(@NonNull Location location) {
                current = location;
                LatLng latLng = new LatLng(location.getCoordinate().getLatitude(),
                        location.getCoordinate().getLongitude());
                if (prev == null) {
                    Bitmap bitmapArrow = BitmapFactory.decodeResource(getResources(), R.drawable.position);
                    Bitmap arrowScaled = Bitmap.createScaledBitmap(bitmapArrow, bitmapArrow.getWidth() / 4, bitmapArrow.getHeight() / 4, false);

                    prev = map.addMarker(new MarkerOptions()
                            .position(latLng)
                            .zIndex(100)
                            .flat(true)
                            .anchor(0.5f, 0.5f)
                            .icon(BitmapDescriptorFactory.fromBitmap(arrowScaled)));
                }
                updateMarkerIcon(location);

                if (location.getQuality() == Location.Quality.LOW) {
                    prev.setPosition(latLng);
                    prev.setRotation((float) location.getBearing().degrees());

                } else {
                    animate(prev, current);
                }


            }

            @Override
            public void onStatusChanged(@NonNull LocationStatus locationStatus) {
                if (!locationManager.isRunning()) {
                    locationManager.removeUpdates(locationListener);
                }
            }

            @Override
            public void onError(@NonNull Error error) {
                Log.e(TAG, "onError: " + error);
            }

        };


//1
        LocationRequest locationRequest = new LocationRequest.Builder()
                .buildingIdentifier(buildingId)
                .useDeadReckoning(true)
                .build();
        SitumSdk.locationManager().requestLocationUpdates(locationRequest, locationListener);

    }

    private void updateMarkerIcon(Location location) {
        boolean newLocationHasOrientation = (location.hasBearing()) && location.isIndoor();
        if (markerWithOrientation == newLocationHasOrientation) {
            return;
        }
        markerWithOrientation = newLocationHasOrientation;

        BitmapDescriptor bitmapDescriptor;
        Bitmap bitmapScaled;
        if(markerWithOrientation){
            Bitmap bitmapArrow = BitmapFactory.decodeResource(getResources(), R.drawable.pose);
            bitmapScaled = Bitmap.createScaledBitmap(bitmapArrow, bitmapArrow.getWidth() / 4,bitmapArrow.getHeight() / 4, false);
        } else {
            Bitmap bitmapCircle = BitmapFactory.decodeResource(getResources(), R.drawable.position);
            bitmapScaled = Bitmap.createScaledBitmap(bitmapCircle, bitmapCircle.getWidth() / 4,bitmapCircle.getHeight() / 4, false);
        }
        bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(bitmapScaled);
        prev.setIcon(bitmapDescriptor);
    }

    private void stopLocation(){
        if (!locationManager.isRunning()){
            return;
        }
        locationManager.removeUpdates(locationListener);
        current = null;
        lastLocation = null;
        if(prev != null){
            prev.remove();
            prev = null;
        }
    }

    synchronized void animate(final Marker marker, final Location location) {
        Coordinate toCoordinate = location.getCoordinate();
        final LatLng toLatLng = new LatLng(toCoordinate.getLatitude(), toCoordinate.getLongitude());
        final float toBearing = (float) location.getBearing().degrees();

        if (lastLocation == null) { //First location
            marker.setRotation(toBearing);
            marker.setPosition(toLatLng);

            lastLocation = location;
            lastLatLng = toLatLng;
            lastBearing = toBearing;
            return;
        }

        animatePosition(marker, location);
        animateBearing(marker, location);
    }


    private void animatePosition(final Marker marker, Location toLocation){
        Coordinate toCoordinate = toLocation.getCoordinate();
        final LatLng toLatLng = new LatLng(toCoordinate.getLatitude(), toCoordinate.getLongitude());

        if ( destinationLatLng != null) {
            float[] results = new float[1];
            android.location.Location.distanceBetween(toCoordinate.getLatitude(), toCoordinate.getLongitude(),
                    destinationLatLng.latitude, destinationLatLng.longitude, results);
            float distance = results[0];
            if (distance < DISTANCE_CHANGE_TO_ANIMATE) {
                return;
            }
        }
        if (destinationLatLng == toLatLng) {
            return;
        }

        locationAnimator.cancel();
        if (lastLocation != toLocation) {

            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
                //hardfix for crash in API 19 at PropertyValuesHolder.setupSetterAndGetter()
                marker.setPosition(toLatLng);
            } else {

                locationAnimator = new ObjectAnimator();
                locationAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                    LatLng startLatLng = lastLatLng;

                    @Override
                    public synchronized void onAnimationUpdate(ValueAnimator animation) {
                        float t = animation.getAnimatedFraction();
                        lastLatLng = interpolateLatLng(t, startLatLng, toLatLng);

                        marker.setPosition(lastLatLng);
                    }
                });
                locationAnimator.setFloatValues(0, 1); //Ignored
                locationAnimator.setDuration(DURATION_POSITION_ANIMATION);
                locationAnimator.start();
            }
            destinationLatLng = toLatLng;
        }
    }

    private LatLng interpolateLatLng(float fraction, LatLng a, LatLng b) {
        double lat = (b.latitude - a.latitude) * fraction + a.latitude;
        double lng = (b.longitude - a.longitude) * fraction + a.longitude;
        return new LatLng(lat, lng);
    }

    private float normalizeAngle(float degrees) {
        degrees = degrees % 360;
        return (degrees + 360) % 360;
    }

    private void animateBearing(final Marker marker, Location location) {
        float degrees = (float) location.getBearing().degrees();

        //Normalize angle
        degrees = normalizeAngle(degrees);
        final float toBearing = degrees;

        if (destinationBearing == toBearing) {
            return;
        }


        locationBearingAnimator.cancel();

        lastBearing =  normalizeAngle(lastBearing);

        //Avoid turning in the wrong direction
        if (lastBearing - toBearing > 180) {
            lastBearing -= 360;
        } else if (toBearing - lastBearing > 180) {
            lastBearing += 360;
        }


        float diffBearing = Math.abs(toBearing - lastBearing);
        if (diffBearing < BEARING_CHANGE_TO_ANIMATE) {
            return;
        }



        if (lastBearing != toBearing) {

            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
                //hardfix for crash in API 19 at PropertyValuesHolder.setupSetterAndGetter()
                marker.setRotation(toBearing);
            } else {

                locationBearingAnimator = new ObjectAnimator();
                locationBearingAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public synchronized void onAnimationUpdate(ValueAnimator animation) {
                        lastBearing = (Float) animation.getAnimatedValue();
                        marker.setRotation(lastBearing);
                    }
                });
                locationBearingAnimator.setFloatValues(lastBearing, toBearing);
                locationBearingAnimator.setDuration(DURATION_BEARING_ANIMATION);
                locationBearingAnimator.start();
            }
            destinationBearing = toBearing;
        }
    }




    // private LocationListener locationListener = new LocationListener() {
    //     @Override
    //     public void onLocationChanged(Location location) {
    //         Log.i(TAG, "onLocationChanged() called with: location = [" + location + "]");
    //     }



    //    @Override
    //    public void onStatusChanged(@NonNull LocationStatus status) {
    //        Log.i(TAG, "onStatusChanged() called with: status = [" + status + "]");
    //    }

    //    @Override
    //     public void onError(@NonNull Error error) {
    //         Log.e(TAG, "onError() called with: error = [" + error + "]");
    //     }
    // };

//1
private void hideProgress(){
    progressBar.setVisibility(View.GONE);
}


}