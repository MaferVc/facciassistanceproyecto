package facci.grupo6.facciassistance;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import facci.grupo6.facciassistance.situm.drawbuilding.Miubicacion;
import facci.grupo6.facciassistance.situm.drawbuilding.realtime.UsuariosActivos;

public class menu_ingreso extends AppCompatActivity {

    ImageView bgapp, clover, miubicacion, activarcuenta, observacion,usuariosactivos;
    LinearLayout textsplash, texthome, menus;
    Animation frombottom;

    private final int REQUEST_ACCESS_FINE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_ingreso);

        frombottom = AnimationUtils.loadAnimation(this, R.anim.frombottom);


        bgapp = (ImageView) findViewById(R.id.splashome);
        clover = (ImageView) findViewById(R.id.clover);
        miubicacion = (ImageView) findViewById(R.id.btnMiUbicacion);
        textsplash = (LinearLayout) findViewById(R.id.textsplash);
        texthome = (LinearLayout) findViewById(R.id.texthome);
        menus = (LinearLayout) findViewById(R.id.menus);
        activarcuenta = (ImageView) findViewById(R.id.btnactivarcuenta);
        observacion = (ImageView) findViewById(R.id.btnobsevaciones);
        usuariosactivos = (ImageView) findViewById(R.id.btnusuariosactivos);

        bgapp.animate().translationY(-1900).setDuration(1500).setStartDelay(300);
        clover.animate().alpha(0).setDuration(1500).setStartDelay(1000);
        textsplash.animate().translationY(140).alpha(0).setDuration(1500).setStartDelay(1000);

        texthome.startAnimation(frombottom);
        menus.startAnimation(frombottom);

        miubicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(menu_ingreso.this, Miubicacion.class);
                startActivity(intent);
            }
        });

        activarcuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(menu_ingreso.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                    ActivityCompat.requestPermissions(menu_ingreso.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_ACCESS_FINE);
            }
        });

        observacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(menu_ingreso.this, observaciones.class);
                startActivity(intent);
            }
        });

        usuariosactivos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(menu_ingreso.this, UsuariosActivos.class);
                startActivity(intent);
            }
        });




    }
    @Override
    public void onRequestPermissionsResult (int requesCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requesCode, permissions, grantResults);

        if (requesCode == REQUEST_ACCESS_FINE) {
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                Toast.makeText(this, "Cuenta activada", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(this, "La cuenta no ha sido activada", Toast.LENGTH_SHORT).show();
        }

    }
}
