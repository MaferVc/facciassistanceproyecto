package facci.grupo6.facciassistance;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class observaciones extends AppCompatActivity {

    Button enviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_observaciones);

        enviar = (Button) findViewById(R.id.btnObservacion);

        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (enviar!=null){
                    Toast.makeText(getApplicationContext(),
                        "Se ha enviado la Observación", Toast.LENGTH_SHORT).show();}
            }
        });

    }

}
