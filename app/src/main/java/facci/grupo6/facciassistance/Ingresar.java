package facci.grupo6.facciassistance;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Ingresar extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingresar);

        Button irRegistrar = (Button) findViewById(R.id.nocuenta);
        final Button Ingresar = (Button) findViewById(R.id.button_signin);

        irRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Ingresar.this, Registrarse.class);
                startActivity(intent);
            }
        });

        Ingresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Ingresar.this, menu_ingreso.class);
                startActivity(intent);
            }
        });
    }
}
